package com.example.bai11;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@RequiredArgsConstructor
public class Bai11Application {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Bai11Application.class, args);
        UserRepository userRepository = context.getBean(UserRepository.class);

        userRepository.findAll();
        User user = userRepository.save(new User());
        System.out.println("User vua luu co ID: "+user.getId());
        Long userId = user.getId();
        user.setAgi(100);
        userRepository.save(user);

        // Query lấy ra user vừa xong để kiểm tra xem.
        User user2 = userRepository.findById(userId).get();
        System.out.println("User: " + user);
        System.out.println("User2: " + user2);

//        // Xóa User khỏi DB
//        userRepository.delete(user);

        // In ra kiểm tra xem userId còn tồn tại trong DB không
        User user3 = userRepository.findById(userId).orElse(null);
        System.out.println("User3: " + user3);
    }

}
